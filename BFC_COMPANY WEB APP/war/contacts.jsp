<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	

	<!-- Facebook APP ID -->
	<meta property="fb:app_id" content="12345" />

	<meta name="keywords" content="Car-Dealer, auto-salon, automobile, business, car, car-gallery, car-selling-template, cars, dealer, marketplace, mobile, real estate, responsive, sell, vehicle" />
	<meta name="description" content="Auto Dealer HTML - Responsive HTML Auto Dealer Template" />

	<!-- Open Graph -->
	<meta property="og:site_name" content="Auto Dealer HTML" />
	<meta property="og:title" content="Contacts" />
	<meta property="og:url" content="http://localhost/02_contacts.jsp" />
	<meta property="og:image" content="http://cdn.winterjuice.com/example/autodealer/image.jpg" />
	<meta property="og:description" content="Auto Dealer HTML - Responsive HTML Auto Dealer Template" />

    <!-- Page Title -->
	<title>Contacts</title>

	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/style980.css" />
	<link rel="stylesheet" type="text/css" href="css/style800.css" />
	<link rel="stylesheet" type="text/css" href="css/style700.css" />
	<link rel="stylesheet" type="text/css" href="css/style600.css" />
	<link rel="stylesheet" type="text/css" href="css/style500.css" />
	<link rel="stylesheet" type="text/css" href="css/style400.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css" media="screen" />
	<!--[if IE]>
	<link href="css/style_ie.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="js/jquery.selectik.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.countdown.js"></script>
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<script type="text/javascript" src="js/js.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="contacts">
	<!--BEGIN HEADER-->
		<div id="header">
			<div class="top_info">
				<div class="logo">
					<a href="#">BFC<span> Company</span></a>
				</div>
				<div class="header_contacts">
					<div class="phone">+94 (071) 636-94-86</div>
					<div>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
				</div>
				<div class="socials">
					<!-- <a href="#"><img src="images/fb_icon.png" alt="" /></a>
					<a href="#"><img src="images/twitter_icon.png" alt="" /></a>
					<a href="#"><img src="images/in_icon.png" alt="" /></a>  -->
				</div>
			</div>
			<div class="bg_navigation">
				<div class="navigation_wrapper">
					<div id="navigation">
						<span>Home</span>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li><a href="05_catalog_grid.jsp">Search Your Car</a></li>
							<li><a href="add.jsp">Add, Order & Delete</a></li>
							<li><a href="contacts.jsp">Contacts</a></li>
						</ul>
					</div>
					<div id="search_form">
						<%
                 		UserService userService = UserServiceFactory.getUserService();
                 		User user = userService.getCurrentUser();
                 		if (user != null) {
                 		pageContext.setAttribute("user", user);
                 		%>
                		<p>Hello, ${fn:escapeXml(user.nickname)}! (You can
                		<a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</p>
                 		<%
                 		} else {
                 		%>
                 		<p>Hello!
                 		<a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
                 		</p>
                 		<%
                    	}
                 		%>
					</div>
				</div>
			</div>
		</div>
	<!--EOF HEADER-->
	<!--BEGIN CONTENT-->
		<div id="content">
			<div class="content">
				<div class="breadcrumbs">
					<a href="./index.jsp">Home</a>
					<img src="images/marker_2.gif" alt="" />
					<span>Contact Us</span>
				</div>
				<div class="main_wrapper">
					<h1><strong>Contact</strong> Us</h1>
					<div class="contacts_box">
						<div class="left">
							<h3><strong>Contact</strong> details</h3>
							<br>
							<h4>If you want to know futher information, come to chat with our sales reps now.</h4>
							<br>
							<div class="addr detail">Bandaranayaka Mawatha, Katubadda,<br /> Moratuwa, Sri Lanka.</div>
							<div class="phones detail">00779327954<br />00716369486</div>
							<div class="email detail single_line"><a href="mailto:#" class="markered">bfc_company@bfc.com</a></div>
							<div class="web detail single_line"><a href="#">http://www.bfc_compaany.com</a></div>
						</div>
						<div class="map">
							<!-- <script id="sid0020000053416551664">(function() {function async_load(){s.id="cid0020000053416551664";s.src='http://st.chatango.com/js/gz/emb.js';s.style.cssText="width:650px;height:350px;";s.async=true;s.text='{"handle":"bfccompany","arch":"js","styles":{"a":"000000","b":100,"c":"FFFFFF","d":"FFFFFF","k":"000000","l":"000000","m":"000000","n":"FFFFFF","p":"11.79","q":"000000","r":100,"usricon":0.94,"surl":0,"cnrs":"0.35"}}';var ss = document.getElementsByTagName('script');for (var i=0, l=ss.length; i < l; i++){if (ss[i].id=='sid0020000053416551664'){ss[i].id +='_';ss[i].parentNode.insertBefore(s, ss[i]);break;}}}var s=document.createElement('script');if (s.async==undefined){if (window.addEventListener) {addEventListener('load',async_load,false);}else if (window.attachEvent) {attachEvent('onload',async_load);}}else {async_load();}})();</script> -->
							<iframe src="http://chatwing.com/chatbox/2c0002b6-1ea4-4c2a-87e5-fb8fd8c59a4e" width="660" height="480" frameborder="1" scrolling="0"></iframe>
							<!-- <img src="images/pics/C5_Showroom.jpg" width="668" height="288" alt="" /> -->
							<!--<iframe width="668" height="288" src="https://maps.google.com.ua/maps?f=q&amp;source=s_q&amp;hl=en;hl=enampamp;hl=en&amp;geocode=&amp;q=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%A1%D0%BE%D0%B5%D0%B4%D0%B8%D0%BD%D0%B5%D0%BD%D0%BD%D1%8B%D0%B5+%D0%A8%D1%82%D0%B0%D1%82%D1%8B+%D0%90%D0%BC%D0%B5%D1%80%D0%B8%D0%BA%D0%B8&amp;aq=0&amp;oq=%D0%BC%D0%B0%D0%BD&amp;sll=46.933064,32.007997&amp;sspn=0.243347,0.676346&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%A1%D0%BE%D0%B5%D0%B4%D0%B8%D0%BD%D1%91%D0%BD%D0%BD%D1%8B%D0%B5+%D0%A8%D1%82%D0%B0%D1%82%D1%8B+%D0%90%D0%BC%D0%B5%D1%80%D0%B8%D0%BA%D0%B8&amp;t=m&amp;ll=40.79042,-73.959961&amp;spn=0.14972,0.457993&amp;z=11&amp;iwloc=A&amp;output=embed"></iframe>-->
						</div>
					</div>
					<div class="contact_form">
						<h2><strong>Drop</strong> us a line</h2>
						<form method="get" action=" " />
							<div class="fld_box">
								<label>Name: </label>
								<input type="text" value="" />
							</div>
							<div class="fld_box center">
								<label>E-mail: </label>
								<input type="text" value="" />
							</div>
							<div class="fld_box last">
								<label>Website: </label>
								<input type="text" value="" />
							</div>
							<div class="clear"></div>
							<label>Message: </label>
							<textarea cols="20" rows="20"></textarea>
							<input type="submit" value="submit" class="submit" />
						</form>
					</div>
					<div class="personal_box">
						<h2><strong>Sales</strong> reps</h2>
						<ul class="personal_list">
							<li>
								<img src="images/pics/Suranga.jpg" alt="" class="thumb" />
								<div class="inner">
									<h4>Suranga Premakumara</h4>
									<div class="grey_area"><span>0094712187816,</span><span>0040 742 016 756</span></div>
									<div class="fL">Location : Kiribathgoda</div>
									<div class="fR"><a href="mailto:#" class="markered">Contact via e-mail </a></div>
								</div>
							</li>
							<li class="last">
								<img src="images/pics/Sachini.jpg" alt="" class="thumb" />
								<div class="inner">
									<h4>Sachini Lanka</h4>
									<div class="grey_area"><span>0094712187816,</span><span>0040 742 016 756</span></div>
									<div class="fL">Location : Moratuwa</div>
									<div class="fR"><a href="mailto:#" class="markered">Contact via e-mail </a></div>
								</div>
							</li>
							<li>
								<img src="images/pics/Sadun.jpg" alt="" class="thumb" />
								<div class="inner">
									<h4>Sadun Tharaka</h4>
									<div class="grey_area"><span>0040 742 016 756,</span><span>0040 742 016 756</span></div>
									<div class="fL">Location : Dehiwala</div>
									<div class="fR"><a href="mailto:#" class="markered">Contact via e-mail </a></div>
								</div>
							</li>
							<li class="last">
								<img src="images/pics/Gayan.jpg" alt="" class="thumb" />
								<div class="inner">
									<h4>Gayan Jayalath</h4>
									<div class="grey_area"><span>0040 742 016 756,</span><span>0040 742 016 756</span></div>
									<div class="fL">Location : Piliyandala</div>
									<div class="fR"><a href="mailto:#" class="markered">Contact via e-mail </a></div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	<!--EOF CONTENT-->
	<!--BEGIN FOOTER-->
		<div id="footer">
			<div class="bg_top_footer">
				<div class="top_footer">
					<div class="f_widget first">
						<h3><strong>About</strong> us</h3>
						<a href="#" class="footer_logo">BFC Company</a>
						<p>We are Batch 11 students of Faculty of information Technology of University of Moratuwa. This is our EAD Assignment, Level3 Semester1 2014. </p>
					</div>
					<div class="f_widget divide second">
						<h3><strong>Open</strong> hours</h3>
						<ul class="schedule">
							<li>
								<strong>Monday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Tuesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Wednesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Thursday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Friday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Saturday</strong>
								<span>9:30 am - 4:00 pm</span>
							</li>
							<li>
								<strong>Sunday</strong>
								<span>closed</span>
							</li>
						</ul>
					</div>
					<div class="fwidget_separator"></div>
					<div class="f_widget third">
						<h3><strong>Our</strong> contacts</h3>
						<div class="f_contact f_contact_1"><strong>Address Info:<br /></strong>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
						<div class="f_contact f_contact_2"><strong>Phone:</strong> +1 (234) 567-8901 <br /><strong>FAX:</strong> +1 (234) 567-8902</div>
						<div class="f_contact f_contact_3"><strong>Email:</strong> <a href="mailto:#">customercare@bfc.com</a></div>
					</div>
					<div class="f_widget divide last frame_wrapper">
						<iframe width="204" height="219" frameborder="0" marginheight="0" marginwidth="0" src="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;output=embed"></iframe><br /><small><a href="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
					</div>
				</div>
			</div>
			<div class="copyright_wrapper">
				<div class="copyright"><span>&copy; 2014 BFC Company. All Rights Reserved.</span></div>
			</div>
		</div>
	<!--EOF FOOTER-->
</body>
</html>
