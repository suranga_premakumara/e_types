<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
	

	<meta name="keywords" content="Car-Dealer, auto-salon, automobile, business, car, car-gallery, car-selling-template, cars, dealer, marketplace, mobile, real estate, responsive, sell, vehicle" />
	<meta name="description" content="Auto Dealer HTML - Responsive HTML Auto Dealer Template" />

	<!-- Page Title -->
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="css/style.css"></link>
	<link rel="stylesheet" type="text/css" href="css/style980.css" />
	<link rel="stylesheet" type="text/css" href="css/style800.css" />
	<link rel="stylesheet" type="text/css" href="css/style700.css" />
	<link rel="stylesheet" type="text/css" href="css/style600.css" />
	<link rel="stylesheet" type="text/css" href="css/style500.css" />
	<link rel="stylesheet" type="text/css" href="css/style400.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css" media="screen" />
	<!--[if IE]>
	<link href="css/style_ie.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="js/jquery.selectik.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.countdown.js"></script>
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<script type="text/javascript" src="js/js.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="page">
	<!--BEGIN HEADER-->
		<div id="header">
			<div class="top_info">
				<div class="logo">
					<a href="#">BFC<span> Company</span></a>
				</div>
				<div class="header_contacts">
					<div class="phone">+94 (071) 636-94-86</div>
					<div>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
				</div>
				<div class="socials">
					<!-- <a href="#"><img src="images/fb_icon.png" alt="" /></a>
					<a href="#"><img src="images/twitter_icon.png" alt="" /></a>
					<a href="#"><img src="images/in_icon.png" alt="" /></a>  -->
				</div>
			</div>
			<div class="bg_navigation">
				<div class="navigation_wrapper">
					<div id="navigation">
						<span>Home</span>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li><a href="05_catalog_grid.jsp">Search Your Car</a></li>
							<li><a href="add.jsp">Add, Order & Delete</a></li>
							<li><a href="contacts.jsp">Contacts</a></li>
						</ul>
					</div>
					<div id="search_form">
						<form method="get" action="./05_catalog_grid.jsp" />
						<!-- <input type="text" onblur="if(this.value=='') this.value='Search on site';" onfocus="if(this.value=='Search on site') this.value='';" value="Search on site" class="txb_search" />
							<input type="submit" value="Search" class="btn_search" /> -->		
						</form>
						<div>
						<%
                 		UserService userService = UserServiceFactory.getUserService();
                 		User user = userService.getCurrentUser();
                 		if (user != null) {
                 		pageContext.setAttribute("user", user);
                 		%>
                		<p>Hello, ${fn:escapeXml(user.nickname)}! (You can
                		<a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</p>
                 		<%
                 		} else {
                 		%>
                 		<p>Hello!
                 		<a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
                 		</p>
                 		<%
                    	}
                 		%>
					</div>
					</div>
				</div>
			</div>
		</div>
	<!--EOF HEADER-->

	<!--BEGIN CONTENT-->
		<div id="content">
			<div class="content">
				<div class="wrapper_1">
					<div class="slider_wrapper"> <b> Latest Cars in 2014...</b>
						<div class="home_slider">
							<div class="slider slider_1">
								<div class="slide">
									<img src="images/pics/Avalon.jpg" alt="" />
									<div class="description">
										<h2 class="title">2014 Toyota Avalon-XLE </h2>
										<p class="desc"><span><strong>Condition: </strong>Brandnew</span><span><strong>Engine: </strong>3.5L</span></p>
										<div class="price">$31,340</div>
									</div>
								</div>
								<div class="slide">
									<img src="images/pics/lexus-ls.jpg" alt="" />
									<div class="description">
										<h2 class="title">2014 Lexus-LS 460 Hybrid</h2>
										<p class="desc"><span><strong>Condition: </strong>Brandnew</span><span><strong>Engine: </strong>5.0L V8</span></p>
										<div class="price">$120,060</div>
									</div>
								</div>
								<div class="slide">
									<img src="images/pics/Pathfinder.jpg" alt="" />
									<div class="description">
										<h2 class="title">2014 Nissan Pathfinder Hybrid SV</h2>
										<p class="desc"><span><strong>Condition: </strong>Brandnew</span><span><strong>Engine: </strong>2.5L</span></p>
										<div class="price">$35,100</div>
									</div>
								</div>
								<div class="slide">
									<img src="images/pics/Civic.jpg" alt="" />
									<div class="description">
										<h2 class="title">2014 Honda Civic Natural</h2>
										<p class="desc"><span><strong>Condition: </strong>Brandnew</span><span><strong>Engine: </strong>1.8L</span></p>
										<div class="price">$26,305</div>
									</div>
								</div>
								<div class="slide">
									<img src="images/pics/BMW.jpg" alt="" />
									<div class="description">
										<h2 class="title">BMW M3 Sedan</h2>
										<p class="desc"><span><strong>Condition: </strong>Brandnew</span><span><strong>Engine: </strong>4.0L V8</span></p>
										<div class="price">$60,000</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="search_auto_wrapper"><br>
						<div class="search_auto">
							<form action="/zzead" method="post" />
								<h3><strong>Search</strong> Your Car</h3>
									<div class="clear"></div>
										<label><strong>Manufacturer:</strong></label>
										<div class="select_box_1">
										<table>
											<tr><td><input type="submit" name="btnHAudi" value="Audi" class="btn_search" /></td></tr>
											<tr><td><input type="submit" name="btnHToyota" value="Toyota" class="btn_search" /></td></tr>
											<tr><td><input type="submit" name="btnHBMW" value="BMW" class="btn_search" /></td></tr>
											<tr><td><input type="submit" name="btnHNissan" value="Nissan" class="btn_search" /></td></tr>
										</table>
										</div>							
							<!-- <input type="submit" value="Search" class="btn_search" /> -->
							</form>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="banners">
					<div class="banner_1 main_banner">
						<div class="text_wrapper">
							<p class="title"><strong>Looking</strong> for a car?</p>
							<p class="desc">10,000 Users every day</p>
						</div>
						<a href="./05_catalog_grid.jsp">Search</a>
					</div>
					<div class="banner_2 main_banner">
						<div class="text_wrapper">
							<p class="title">Can't select a car?</p>
							<p class="desc">Contact our salesman via Google chat Now!</p>
						</div>
						<a href="./contacts.jsp">Chat Now!</a>
					</div>
				</div>
		</div>
	<!--EOF CONTENT-->
	<!--BEGIN FOOTER-->
		<div id="footer">
			<div class="bg_top_footer">
				<div class="top_footer">
					<div class="f_widget first">
						<h3><strong>About</strong> us</h3>
						<a href="#" class="footer_logo">BFC Company</a>
						<p>We are Batch 11 students of Faculty of information Technology of University of Moratuwa. This is our EAD Assignment, Level3 Semester1 2014. </p>
					</div>
					<div class="f_widget divide second">
						<h3><strong>Open</strong> hours</h3>
						<ul class="schedule">
							<li>
								<strong>Monday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Tuesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Wednesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Thursday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Friday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Saturday</strong>
								<span>9:30 am - 4:00 pm</span>
							</li>
							<li>
								<strong>Sunday</strong>
								<span>closed</span>
							</li>
						</ul>
					</div>
					<div class="fwidget_separator"></div>
					<div class="f_widget third">
						<h3><strong>Our</strong> contacts</h3>
						<div class="f_contact f_contact_1"><strong>Address Info:<br /></strong>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
						<div class="f_contact f_contact_2"><strong>Phone:</strong> +1 (234) 567-8901 <br /><strong>FAX:</strong> +1 (234) 567-8902</div>
						<div class="f_contact f_contact_3"><strong>Email:</strong> <a href="mailto:#">customercare@bfc.com</a></div>
					</div>
					<div class="f_widget divide last frame_wrapper">
						<iframe width="204" height="219" frameborder="0" marginheight="0" marginwidth="0" src="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;output=embed"></iframe><br /><small><a href="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
					</div>
				</div>
			</div>
			<div class="copyright_wrapper">
				<div class="copyright"><span>&copy; 2014 BFC Company. All Rights Reserved.</span></div>
			</div>
		</div>
	<!--EOF FOOTER-->
</body>
</html>
