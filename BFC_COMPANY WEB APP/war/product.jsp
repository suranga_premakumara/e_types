<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	

	<!-- Facebook APP ID -->
	<meta property="fb:app_id" content="12345" />

	<meta name="keywords" content="Car-Dealer, auto-salon, automobile, business, car, car-gallery, car-selling-template, cars, dealer, marketplace, mobile, real estate, responsive, sell, vehicle" />
	<meta name="description" content="Auto Dealer HTML - Responsive HTML Auto Dealer Template" />

	<!-- Open Graph -->
	<meta property="og:site_name" content="Auto Dealer HTML" />
	<meta property="og:title" content="Product Page" />
	<meta property="og:url" content="http://localhost/06_product_page.html" />
	<meta property="og:image" content="http://cdn.winterjuice.com/example/autodealer/image.jpg" />
	<meta property="og:description" content="Auto Dealer HTML - Responsive HTML Auto Dealer Template" />

    <!-- Page Title -->
	<title>Car</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/style980.css" />
	<link rel="stylesheet" type="text/css" href="css/style800.css" />
	<link rel="stylesheet" type="text/css" href="css/style700.css" />
	<link rel="stylesheet" type="text/css" href="css/style600.css" />
	<link rel="stylesheet" type="text/css" href="css/style500.css" />
	<link rel="stylesheet" type="text/css" href="css/style400.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css" media="screen" />
	<!--[if IE]>
	<link href="css/style_ie.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="js/jquery.selectik.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.countdown.js"></script>
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<script type="text/javascript" src="js/js.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="car">
	<!--BEGIN HEADER-->
		<div id="header">
			<div class="top_info">
				<div class="logo">
					<a href="#">BFC<span> Company</span></a>
				</div>
				<div class="header_contacts">
					<div class="phone">+94 (071) 636-94-86</div>
					<div>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
				</div>
				<div class="socials">
				</div>
			</div>
			<div class="bg_navigation">
				<div class="navigation_wrapper">
					<div id="navigation">
						<span>Home</span>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li><a href="05_catalog_grid.jsp">Search Your Car</a></li>
							<li><a href="add.jsp">Add, Order & Delete</a></li>
							<li><a href="contacts.jsp">Contacts</a></li>
						</ul>
					</div>
					<div id="search_form">
						<%
                 		UserService userService = UserServiceFactory.getUserService();
                 		User user = userService.getCurrentUser();
                 		if (user != null) {
                 		pageContext.setAttribute("user", user);
                 		%>
                		<p>Hello, ${fn:escapeXml(user.nickname)}! (You can
                		<a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</p>
                 		<%
                 		} else {
                 		%>
                 		<p>Hello!
                 		<a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
                 		</p>
                 		<%
                    	}
                 		%>
					</div>
				</div>
			</div>
		</div>
	<!--EOF HEADER-->
	<!--BEGIN CONTENT-->
		<div id="content">
			<div class="content">
				<div class="breadcrumbs">
					<a href="./index.jsp">Home</a>
					<img src="images/marker_2.gif" alt="" />
					<a href="#">Cars</a>
					<img src="images/marker_2.gif" alt="" />
					<a href="#">Mercedes-Benz</a>
					<img src="images/marker_2.gif" alt="" />
					<span>Mercedes-Benz CLS 320</span>
				</div>
				<div class="main_wrapper">
					<h1><strong>Mercedes-Benz</strong> CLS 320</h1>
					<div class="car_image_wrapper">
						<div class="big_image">
							<a href="images/pics/car_1.jpg" rel="car_group">
								<img src="images/zoom.png" alt="" class="zoom" />
								<img src="images/pics/car_1.jpg" alt="" />
							</a>
						</div>
						<div class="small_img">
							<a href="images/pics/slider_img_5.jpg" rel="car_group">
								<img src="images/pics/car_2.jpg" alt="" />
							</a>
							<a href="images/pics/slider_img_1.jpg" rel="car_group">
								<img src="images/pics/car_3.jpg" alt="" />
							</a>
							<a href="images/pics/slider_img_2.jpg" rel="car_group">
								<img src="images/pics/car_4.jpg" alt="" />
							</a>
							<a href="images/pics/slider_img_3.jpg" rel="car_group">
								<img src="images/pics/car_5.jpg" alt="" />
							</a>
							<a href="images/pics/slider_img_4.jpg" rel="car_group">
								<img src="images/pics/car_6.jpg" alt="" />
							</a>
						</div>
					</div>
					<div class="car_characteristics">
						<a href="#" class="print"></a>
						<div class="price">54.980 EURO <span>* Price negotiable</span></div>
						<div class="clear"></div>
						<div class="features_table">
							<div class="line grey_area">
								<div class="left">Model, Body type:</div>
								<div class="right">Mercedes-Benz CLS 320, Coupe</div>
							</div>
							<div class="line">
								<div class="left">Fabrication:</div>
								<div class="right">2010</div>
							</div>
							<div class="line grey_area">
								<div class="left">Fuel:</div>
								<div class="right">Diesel</div>
							</div>
							<div class="line">
								<div class="left">Engine:</div>
								<div class="right">3200 cmÂ³ (373 kW / 507 CP)</div>
							</div>
							<div class="line grey_area">
								<div class="left">Transmision:</div>
								<div class="right">Automatic</div>
							</div>
							<div class="line">
								<div class="left">Color:</div>
								<div class="right">Black</div>
							</div>
							<div class="line grey_area">
								<div class="left">Doors:</div>
								<div class="right">4/5</div>
							</div>
							<div class="line">
								<div class="left">CO2-Emissions combined:</div>
								<div class="right">ca 423 g/km</div>
							</div>
							<div class="line grey_aea">
								<form action="/zzead" method="post">
									<div class="left"><input style="font-size: x-large;" type="submit" name="btnBuyCar" value="Buy Car" class="btn_search" /></div>
								</form>
							</div>
						</div>
						<div class="wanted_line">
						</div>
					</div>
					<div class="clear"></div>
					<div class="info_box">
						<div class="car_info">
							<div class="info_left">
								<h2><strong>Vehicle</strong> information</h2>
								<p><strong>Features:</strong><br />alloy wheels, xenon headlights, fog lights, tinted glass</p>
								<p><strong>Other parameters:</strong><br />service book</p>
								<p><strong>Safety:</strong><br />ABS, traction control, alarm, airbags, immobilizer, anti-th, ESP, EDS, protection framework</p>
								<p><strong>Comfort:</strong><br />electric windows, electric mirrors, air conditioning, leathe upholstery, navigation system, central locking, radio / CD, power steering, onboard computer, cruise control, heated seats, rain sensor, steering wheel controls, parking sensor</p>
							</div>
							<div class="info_right">
								<h2><strong>More</strong> info</h2>
								<p class="first"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean aliquet fringilla metus, a ultricies ligula consequa at maecenas eget massa at eros.</strong></p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit aenean aliquet fringilla metus, a ultricies ligula consequa at. Maecenas eget massa at eros ornare rhoncus. In sit a enim risus, in mattis felis a ultricies ligula consequat at. Maecenas eget massa at eros ornare.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean aliquet fringilla metus, a ultricies ligula consequa at. Maecenas eget massa at eros ornare rhoncus. In sit a enim risus, in mattis felis</p>
							</div>
							<div class="clear"></div>
						</div>
						<div class="car_contacts">
							
						</div>
					</div>
					<div class="car_sidebar">
						<div class="banner">
              <br>
							<h2><strong>Contact</strong> details</h2>
							<p>AutoMarket does not store additional information about the seller except for those contained in the announcement.</p>
							<div class="left">
								<div class="phones detail single_line spaced">0040 742 016 756</div>
								<div class="email detail single_line"><a href="mailto:#" class="markered">Contact vendor via e-mail</a></div>
							</div>
							<div class="right">
								<div class="addr detail single_line">Berlin, Germany, nr. 250330, main street</div>
								<div class="web detail single_line"><a href="#">http://www.dealer.automarket.com</a></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="recent_cars">
						<h2><strong>Similar</strong> offers</h2>
						<ul>
							<li>
								<a href="product">
									<img src="images/pics/recent_1.jpg" alt="" />
									<div class="description">Registration 2010<br />3.0 Diesel<br />230 HP<br />Body Coupe<br />80 000 Miles</div>
									<div class="title">Mercedes-Benz <span class="price">$ 115 265</span></div>
								</a>
							</li>
							<li>
								<a href="product">
									<img src="images/pics/recent_1.jpg" alt="" />
									<div class="description">Registration 2010<br />3.0 Diesel<br />230 HP<br />Body Coupe<br />80 000 Miles</div>
									<div class="title">Mercedes-Benz <span class="price">$ 115 265</span></div>
								</a>
							</li>
							<li>
								<a href="product">
									<img src="images/pics/recent_1.jpg" alt="" />
									<div class="description">Registration 2010<br />3.0 Diesel<br />230 HP<br />Body Coupe<br />80 000 Miles</div>
									<div class="title">Mercedes-Benz <span class="price">$ 115 265</span></div>
								</a>
							</li>
							<li class="last">
								<a href="product">
									<img src="images/pics/recent_1.jpg" alt="" />
									<div class="description">Registration 2010<br />3.0 Diesel<br />230 HP<br />Body Coupe<br />80 000 Miles</div>
									<div class="title">Mercedes-Benz <span class="price">$ 115 265</span></div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	<!--EOF CONTENT-->
<!--BEGIN FOOTER-->
		<div id="footer">
			<div class="bg_top_footer">
				<div class="top_footer">
					<div class="f_widget first">
						<h3><strong>About</strong> us</h3>
						<a href="#" class="footer_logo">BFC Company</a>
						<p>We are Batch 11 students of Faculty of information Technology of University of Moratuwa. This is our EAD Assignment, Level3 Semester1 2014. </p>
					</div>
					<div class="f_widget divide second">
						<h3><strong>Open</strong> hours</h3>
						<ul class="schedule">
							<li>
								<strong>Monday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Tuesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Wednesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Thursday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Friday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Saturday</strong>
								<span>9:30 am - 4:00 pm</span>
							</li>
							<li>
								<strong>Sunday</strong>
								<span>closed</span>
							</li>
						</ul>
					</div>
					<div class="fwidget_separator"></div>
					<div class="f_widget third">
						<h3><strong>Our</strong> contacts</h3>
						<div class="f_contact f_contact_1"><strong>Address Info:<br /></strong>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
						<div class="f_contact f_contact_2"><strong>Phone:</strong> +1 (234) 567-8901 <br /><strong>FAX:</strong> +1 (234) 567-8902</div>
						<div class="f_contact f_contact_3"><strong>Email:</strong> <a href="mailto:#">customercare@bfc.com</a></div>
					</div>
					<div class="f_widget divide last frame_wrapper">
						<iframe width="204" height="219" frameborder="0" marginheight="0" marginwidth="0" src="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;output=embed"></iframe><br /><small><a href="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
					</div>
				</div>
			</div>
			<div class="copyright_wrapper">
				<div class="copyright"><span>&copy; 2014 BFC Company. All Rights Reserved.</span></div>
			</div>
		</div>
	<!--EOF FOOTER-->
</body>
</html>
