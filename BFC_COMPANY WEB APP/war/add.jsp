<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	

	<!-- Facebook APP ID -->
	<meta property="fb:app_id" content="12345" />

	<meta name="keywords" content="Car-Dealer, auto-salon, automobile, business, car, car-gallery, car-selling-template, cars, dealer, marketplace, mobile, real estate, responsive, sell, vehicle" />
	<meta name="description" content="Auto Dealer HTML - Responsive HTML Auto Dealer Template" />

	<!-- Open Graph -->
	<meta property="og:site_name" content="Auto Dealer HTML" />
	<meta property="og:title" content="Add Product" />
	<meta property="og:url" content="http://localhost/07_add.jsp" />
	<meta property="og:image" content="http://cdn.winterjuice.com/example/autodealer/image.jpg" />
	<meta property="og:description" content="Auto Dealer HTML - Responsive HTML Auto Dealer Template" />

    <!-- Page Title -->
	<title>Add</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/style980.css" />
	<link rel="stylesheet" type="text/css" href="css/style800.css" />
	<link rel="stylesheet" type="text/css" href="css/style700.css" />
	<link rel="stylesheet" type="text/css" href="css/style600.css" />
	<link rel="stylesheet" type="text/css" href="css/style500.css" />
	<link rel="stylesheet" type="text/css" href="css/style400.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css" media="screen" />
	<!--[if IE]>
	<link href="css/style_ie.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="js/jquery.selectik.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.countdown.js"></script>
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<script type="text/javascript" src="js/js.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="sell">
	<!--BEGIN HEADER-->
		<div id="header">
			<div class="top_info">
				<div class="logo">
					<a href="#">BFC<span> Company</span></a>
				</div>
				<div class="header_contacts">
					<div class="phone">+94 (071) 636-94-86</div>
					<div>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
				</div>
			</div>
			<div class="bg_navigation">
				<div class="navigation_wrapper">
					<div id="navigation">
						<span>Home</span>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li><a href="05_catalog_grid.jsp">Search Your Car</a></li>
							<li><a href="add.jsp">Add, Order & Delete</a></li>
							<li><a href="contacts.jsp">Contacts</a></li>
						</ul>
					</div>
					<div id="search_form">
						<%
                 		UserService userService = UserServiceFactory.getUserService();
                 		User user = userService.getCurrentUser();
                 		if (user != null) {
                 		pageContext.setAttribute("user", user);
                 		%>
                		<p>Hello, ${fn:escapeXml(user.nickname)}! (You can <a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</p>
                 		<%
                 		} else {
                 		%>
                 		<p>Hello!
                 		<a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
                 		</p>
                 		<%
                    	}
                 		%>
					</div>
				</div>
			</div>
		</div>
	<!--EOF HEADER-->
	<!--BEGIN CONTENT-->
		<div id="content">
			<div class="content">
				<div class="breadcrumbs">
					
				</div>
				<div class="main_wrapper">
					<h1><strong>Ordering, Adding & Deleting </strong>Cars</h1>
					<div class="message">
            			<h3>This option is only for <strong>BFC Company</strong> administrators only!</h3>
					</div>
					<div class="sell_box sell_box_1">
						<h2><strong>Ordering</strong> Cars</h2>
							<form action="/zzead" method="post">
								<table>
									<tr>	
										<td><div class="input_wrapper"><label><span></span><strong>Audi Orders: </strong></label><input type="text" class="txb" value="0" name="txtAudi" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>Toyota Orders: </strong></label><input type="text" class="txb" value="0" name="txtToyota" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>BMW Orders: </strong></label><input type="text" class="txb" value="0" name="txtBMW" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>Nissan Orders: </strong></label><input type="text" class="txb" value="0" name="txtNissan" /></div></td>
									</tr>
									<tr>
										<p><br></p>
										<td><input type="submit" name="btnOrder" value="Place Order" class="btn_search" /><p></p></td>
									</tr>
								</table>
							</form>		
						<div class="clear"></div>
						<div class="message">
            			<h3></h3>
						</div>
						<h2><strong>Adding</strong> Cars</h2>
							<form action="/zzead" method="post">
								<table>
									<tr>	
										<td><div class="input_wrapper"><label><span></span><strong>Fuel Type: </strong></label><input type="text" class="txb" value="0" name="txtFuelType" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>Capacity: </strong></label><input type="text" class="txb" value="0" name="txtCapacity" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>Engine ID: </strong></label><input type="text" class="txb" value="0" name="txtEngineId" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>Price: </strong></label><input type="text" class="txb" value="0" name="txtPrice" /></div></td>
									</tr>
									<tr>
										<p> <br></p>
										<td><input type="submit" name="btnAddAudi" value="Add Audi Car" class="btn_search" /></td>
										<td><input type="submit" name="btnAddToyota" value="Add Toyota Car" class="btn_search" /></td>
										<td><input type="submit" name="btnAddBMW" value="Add BMW Car" class="btn_search" /></td>
										<td><input type="submit" name="btnAddNissan" value="Add Nissan Car" class="btn_search" /></td>
									</tr>
								</table>
							</form>		
						<div class="clear"></div>
						<div class="message">
            			<h3></h3>
            			</div>
						<h2><strong>Deleting</strong> Cars</h2>
							<form action="/zzead" method="post">
								<table>
									<tr>	
										<td><div class="input_wrapper"><label><span></span><strong>Audi Car Engine ID: </strong></label><input type="text" class="txb" value="0" name="txtDelAudi" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>Toyota Car Engine ID: </strong></label><input type="text" class="txb" value="0" name="txtDelToyota" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>BMW Car Engine ID: </strong></label><input type="text" class="txb" value="0" name="txtDelBMW" /></div></td>
										<td><div class="input_wrapper"><label><span></span><strong>Nissan Car Engine ID: </strong></label><input type="text" class="txb" value="0" name="txtDelNissan" /></div></td>
									</tr>
									<tr>
										<p> <br></p>
										<td><input type="submit" name="btnDelAudi" value="Delete Audi Car" class="btn_search" /></td>
										<td><input type="submit" name="btnDelToyota" value="Delete Toyota Car" class="btn_search" /></td>
										<td><input type="submit" name="btnDelBMW" value="Delete BMW Car" class="btn_search" /></td>
										<td><input type="submit" name="btnDelNissan" value="Delete Nissan Car" class="btn_search" /></td>
									</tr>
								</table>
							</form>	
						<div class="clear"></div>
					</div>		
				</div>
			</div>
		</div>
	<!--EOF CONTENT-->
	<!--BEGIN FOOTER-->
		<div id="footer">
			<div class="bg_top_footer">
				<div class="top_footer">
					<div class="f_widget first">
						<h3><strong>About</strong> us</h3>
						<a href="#" class="footer_logo">BFC Company</a>
						<p>We are Batch 11 students of Faculty of information Technology of University of Moratuwa. This is our EAD Assignment, Level3 Semester1 2014. </p>
					</div>
					<div class="f_widget divide second">
						<h3><strong>Open</strong> hours</h3>
						<ul class="schedule">
							<li>
								<strong>Monday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Tuesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Wednesday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Thursday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Friday</strong>
								<span>9:30 am - 6:00 pm</span>
							</li>
							<li>
								<strong>Saturday</strong>
								<span>9:30 am - 4:00 pm</span>
							</li>
							<li>
								<strong>Sunday</strong>
								<span>closed</span>
							</li>
						</ul>
					</div>
					<div class="fwidget_separator"></div>
					<div class="f_widget third">
						<h3><strong>Our</strong> contacts</h3>
						<div class="f_contact f_contact_1"><strong>Address Info:<br /></strong>Bandaranayaka Mawatha, Katubadda, Moratuwa, Sri Lanka.</div>
						<div class="f_contact f_contact_2"><strong>Phone:</strong> +1 (234) 567-8901 <br /><strong>FAX:</strong> +1 (234) 567-8902</div>
						<div class="f_contact f_contact_3"><strong>Email:</strong> <a href="mailto:#">customercare@bfc.com</a></div>
					</div>
					<div class="f_widget divide last frame_wrapper">
						<iframe width="204" height="219" frameborder="0" marginheight="0" marginwidth="0" src="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;output=embed"></iframe><br /><small><a href="https://maps.google.lk/maps?ie=UTF8&amp;q=university+of+moratuwa&amp;fb=1&amp;gl=lk&amp;hq=moratuwa+university&amp;cid=8922518312307545614&amp;ll=6.796877,79.901778&amp;spn=0.006295,0.006295&amp;t=m&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
					</div>
				</div>
			</div>
			<div class="copyright_wrapper">
				<div class="copyright"><span>&copy; 2014 BFC Company. All Rights Reserved.</span></div>
			</div>
		</div>
	<!--EOF FOOTER-->
</body>
</html>
