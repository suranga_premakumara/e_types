package com.Controler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

@SuppressWarnings("serial")
public class ZZEADServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		
		DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
		
		
        ////////////////////////////////////////////////CAR DISPLAYING///////////////////////////////////////////////////////////////
		if ((req.getParameter("btnAudi") != null) || (req.getParameter("btnHAudi") != null) ) {
			String data = "<h1>Audi Cars</h1>";
			int i =1;
			Query q = new Query("AudiCar"); 
	        PreparedQuery pq = ds.prepare(q);  
	        for (Entity result : pq.asIterable()) {   
	        	data = data +"<br><h3>"+i+") "+"<a href=\"product.jsp\">"+ (String) result.getProperty("Capacity")+", "+ (String) result.getProperty("Fuel_Type")+", " + (String) result.getProperty("Price")+", EngineID: "+ (String) result.getProperty("Engine_ID") +"</a>"+"</h3>";
	        	i++;
	        }
	        req.setAttribute("listData", data);
	        req.getRequestDispatcher("05_catalog_grid.jsp").forward(req, resp);

		} else if ((req.getParameter("btnToyota") != null)|| (req.getParameter("btnHToyota") != null) ) {
			String data = "<h1>Toyota Cars</h1>";
			int i =1;
			Query q = new Query("ToyotaCar"); 
	        PreparedQuery pq = ds.prepare(q);  
	        for (Entity result : pq.asIterable()) {   
	        	data = data +"<br><h3>"+i+") "+"<a href=\"product.jsp\">"+ (String) result.getProperty("Capacity")+", "+ (String) result.getProperty("Fuel_Type")+", " + (String) result.getProperty("Price")+", EngineID: "+ (String) result.getProperty("Engine_ID") +"</a>"+"</h3>";
	        	i++;
	        }
	        req.setAttribute("listData", data);
	        req.getRequestDispatcher("05_catalog_grid.jsp").forward(req, resp);


		} else if ((req.getParameter("btnBMW") != null)|| (req.getParameter("btnHBMW") != null) ) {
			String data = "<h1>BMW Cars</h1>";
			int i =1;
			Query q = new Query("BMWCar"); 
	        PreparedQuery pq = ds.prepare(q);  
	        for (Entity result : pq.asIterable()) {   
	        	data = data +"<br><h3>"+i+") "+"<a href=\"product.jsp\">"+ (String) result.getProperty("Capacity")+", "+ (String) result.getProperty("Fuel_Type")+", " + (String) result.getProperty("Price")+", EngineID: "+ (String) result.getProperty("Engine_ID") +"</a>"+"</h3>";
	        	i++;
	        }
	        req.setAttribute("listData", data);
	        req.getRequestDispatcher("05_catalog_grid.jsp").forward(req, resp);

			
		} else if ((req.getParameter("btnNissan") != null)|| (req.getParameter("btnHNissan") != null) ) {
			String data = "<h1>Nissan Cars</h1>";
			int i =1;
			Query q = new Query("NissanCar"); 
	        PreparedQuery pq = ds.prepare(q);  
	        for (Entity result : pq.asIterable()) {   
	        	data = data +"<br><h3>"+i+") "+"<a href=\"product.jsp\">"+ (String) result.getProperty("Capacity")+", "+ (String) result.getProperty("Fuel_Type")+", " + (String) result.getProperty("Price")+", EngineID: "+ (String) result.getProperty("Engine_ID") +"</a>"+"</h3>";
	        	i++;
	        }
	        req.setAttribute("listData", data);
	        req.getRequestDispatcher("05_catalog_grid.jsp").forward(req, resp);		
		}
        ////////////////////////////////////////////////CAR DISPLAYING///////////////////////////////////////////////////////////////

		
        ////////////////////////////////////////////////CAR ORDERING PLACEMENT////////////////////////////////////////////////////////
		if (req.getParameter("btnOrder") != null){
        	String Audi  = req.getParameter("txtAudi");
        	String Toyota  = req.getParameter("txtToyota");
        	String BMW  = req.getParameter("txtBMW");
        	String Nissan  = req.getParameter("txtNissan");
        	
        	JSONObject json = new JSONObject();
        	try {
				json.put( "Audi_Engines", Audi);
				json.put( "Toyota_Engines", Toyota);
				json.put( "BMW_Engines", BMW);
				json.put( "Nissan_Engines", Nissan);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
        	
        	resp.setContentType("text/plain");
    		resp.getWriter().println(json.toString());	
		}
        ////////////////////////////////////////////////CAR ORDERING PLACEMENT/////////////////////////////////////////////////////
    	
    		
    	////////////////////////////////////////////////CAR ADDING/////////////////////////////////////////////////////////////////
    	if (req.getParameter("btnAddAudi") != null){
    		Entity ACar=new Entity("AudiCar",req.getParameter("txtEngineId"));
            ACar.setProperty("Engine_ID",req.getParameter("txtEngineId"));
            ACar.setProperty("Fuel_Type",req.getParameter("txtFuelType"));
            ACar.setProperty("Capacity",req.getParameter("txtCapacity"));
            ACar.setProperty("Price",req.getParameter("txtPrice"));
            ds.put(ACar);
            resp.setContentType("text/plain");
    		resp.getWriter().println("Successfully Adding");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);
    		
    	} else if (req.getParameter("btnAddToyota") != null) {
    		Entity TCar=new Entity("ToyotaCar",req.getParameter("txtEngineId"));
            TCar.setProperty("Engine_ID",req.getParameter("txtEngineId"));
            TCar.setProperty("Fuel_Type",req.getParameter("txtFuelType"));
            TCar.setProperty("Capacity",req.getParameter("txtCapacity"));
            TCar.setProperty("Price",req.getParameter("txtPrice"));
            ds.put(TCar);
            resp.setContentType("text/plain");
    		resp.getWriter().println("Successfully Adding");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);

		} else if (req.getParameter("btnAddBMW") != null) {
			Entity BCar=new Entity("BMWCar",req.getParameter("txtEngineId"));
            BCar.setProperty("Engine_ID",req.getParameter("txtEngineId"));
            BCar.setProperty("Fuel_Type",req.getParameter("txtFuelType"));
            BCar.setProperty("Capacity",req.getParameter("txtCapacity"));
            BCar.setProperty("Price",req.getParameter("txtPrice"));
            ds.put(BCar);
            resp.setContentType("text/plain");
    		resp.getWriter().println("Successfully Adding");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);

		} else if (req.getParameter("btnAddNissan") != null) {
			Entity NCar=new Entity("NissanCar",req.getParameter("txtEngineId"));
            NCar.setProperty("Engine_ID",req.getParameter("txtEngineId"));
            NCar.setProperty("Fuel_Type",req.getParameter("txtFuelType"));
            NCar.setProperty("Capacity",req.getParameter("txtCapacity"));
            NCar.setProperty("Price",req.getParameter("txtPrice"));
            ds.put(NCar);
            resp.setContentType("text/plain");
    		resp.getWriter().println("Successfully Adding");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);			
		}
    	////////////////////////////////////////////////CAR ADDING////////////////////////////////////////////////////////////////
    	
    	
    	///////////////////////////////////////////////CAR DELETING///////////////////////////////////////////////////////////////
    	if(req.getParameter("btnDelAudi") != null){
    		String delCarID1 = req.getParameter("txtDelAudi");   		
    		Key key =KeyFactory.createKey("AudiCar",delCarID1);
    		ds.delete(key);	
    		resp.getWriter().println("Successfully Deleted");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);			
    	}
    	else if(req.getParameter("btnDelToyota") != null){
    		String delCarID2 = req.getParameter("txtDelToyota");   		
    		Key key =KeyFactory.createKey("ToyotaCar",delCarID2);
    		ds.delete(key);	
    		resp.getWriter().println("Successfully Deleted");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);			
    	}
    	else if(req.getParameter("btnDelBMW") != null){
    		String delCarID3 = req.getParameter("txtDelBMW");	   		
    		Key key =KeyFactory.createKey("BMWCar",delCarID3);
    		ds.delete(key);	
    		resp.getWriter().println("Successfully Deleted");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);			
    	}
    	else if(req.getParameter("btnDelNissan") != null){
    		String delCarID4 = req.getParameter("txtDelNissan");   		   		
    		Key key =KeyFactory.createKey("NissanCar",delCarID4);
    		ds.delete(key);
    		resp.getWriter().println("Successfully Deleted");	
            req.getRequestDispatcher("add.jsp").forward(req, resp);			
    	}
    	///////////////////////////////////////////////CAR DELETING///////////////////////////////////////////////////////////////
    	
    	
		
	}
}
